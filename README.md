# PISA Dashboard

## Development

This project uses [Poetry](https://python-poetry.org/).

To run the project in development mode, execute:

```export
export FLASK_APP=hello_world
export FLASK_ENV=development

poetry run flask run
```

